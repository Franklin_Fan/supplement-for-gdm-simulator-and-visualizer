# README #

 This repository contains the documentation, data sets for the ROS packages gas_dispersion_simulation and gas_concentration_visualization. 

### What is this gas_dispersion_simulation and gas_concentration_visualization for? ###

* ROS packages gas_dispersion_simulation and gas_concentration_visualization are developed for simulating gas dispersion and visualizing the corresponding concentration distribution.
* Version 2.2

### How do I get set up? ###

* Summary of set up is provided in Install_guide.pdf in the fold Doc of this repository.
* The demo examples are provided in the packages gas_dispersion_simulation and gas_concentration_visualization in the repositories of gas_dispersion_simulation and gas_dispersion_viz respectively. Corresponding instructions can be found in Install_guide.pdf.


### Who do I talk to? ###

* Han Fan (han.fan@oru.se)